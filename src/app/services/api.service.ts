import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { IMovie } from '../interfaces/interfaces';
import { environment } from '../../environments/environment';

@Injectable()
export class ApiService {

  constructor(
      private readonly _http: HttpClient
  ) { }

    /**
     * fetches list of all movies available
     * @returns void
     */
  public getMovies(): Observable<IMovie[]> {
    return this._http.get<IMovie[]>(`${environment.apiUrl}movies`);
  }

    /**
     * fetches movie specified with id
     * @param {string} id - movie id
     * @returns {Observable<IMovie>}
     */
  public getMovieById(id: string): Observable<IMovie> {
    return this._http.get<IMovie>(`${environment.apiUrl}movies/${id}`);
  }

}
