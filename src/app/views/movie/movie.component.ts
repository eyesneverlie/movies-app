import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ApiService } from '../../services/api.service';
import { IMovie } from '../../interfaces/interfaces';
import { Location } from '@angular/common';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit, OnDestroy {
  public currentMovieId: string;
  public currentMovie: IMovie;
  public subscriptions: Subscription[] = [];

  constructor(
      private readonly _route: ActivatedRoute,
      private readonly _apiService: ApiService,
      private readonly _location: Location
  ) { }

    /**
     * init function
     * @returns void
     */
  public ngOnInit(): void {
    this._route.params
      .subscribe((params) => {
        this.currentMovieId = params['id'];
        this.getCurrentMovie();
      });
  }

    /**
     * fetches movie by specified id
     * @returns void
     */
  public getCurrentMovie(): void {
    this.subscriptions.push(
      this._apiService.getMovieById(this.currentMovieId)
        .subscribe((movie) => {
          this.currentMovie = movie;
        })
    );
  }

    /**
     * destroys all subscriptions
     * @returns void
     */
  public ngOnDestroy(): void {
      this.subscriptions.forEach((s) => s.unsubscribe());
  }

    /**
     * returns to previously visited page
     * @returns void
     */
  public returnToPreviousPage(): void {
      this._location.back();

  }

}
