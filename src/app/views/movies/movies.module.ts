import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoviesRoutingModule } from './movies-routing.module';
import { MoviesComponent } from './movies.component';
import { MovieComponent } from '../movie/movie.component';

import { ApiService } from '../../services/api.service';
import { JoinPipe } from '../../pipes/join.pipe';
import { FormsModule } from '@angular/forms';
import { TruncatePipe } from '../../pipes/truncate.pipe';

@NgModule({
  imports: [
    CommonModule,
    MoviesRoutingModule,
    FormsModule,
  ],
  declarations: [
    MoviesComponent,
    MovieComponent,
    JoinPipe,
    TruncatePipe
  ],
  providers: [
    ApiService,
  ]
})
export class MoviesModule { }
