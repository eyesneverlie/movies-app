import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MoviesComponent } from './movies.component';
import { MovieComponent } from '../movie/movie.component';

const moviesRoutes: Routes = [
  { path: '', component: MoviesComponent },
  { path: ':id', component: MovieComponent },
];

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(moviesRoutes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class MoviesRoutingModule { }
