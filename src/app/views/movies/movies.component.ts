import { Component, OnDestroy, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { IMovie } from '../../interfaces/interfaces';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit, OnDestroy {
  public moviesList: IMovie[] = [];
  public moviesMatchingCriteria: IMovie[] = [];
  public subscriptions: Subscription[] = [];
  public searchCriteriaChanged: Subject<string> = new Subject<string>();
  public genreCriteriaChanged: Subject<string> = new Subject<string>();
  public currentGenre = '';
  public currentSearchCriteria = '';

  constructor(
      private readonly _api: ApiService,
  ) {
      // detect changes in search input
      this.searchCriteriaChanged
          .debounceTime(300) // wait 300ms after the last event before emitting last event
          .distinctUntilChanged() // only emit if value is different from previous value
          .subscribe(criteria => {
              if (!this.currentGenre) {
                  if (criteria) {
                      this.moviesMatchingCriteria = this.moviesList.filter(
                          (movie) => movie.name.toLowerCase().indexOf(criteria) !== -1);
                  } else {
                      this.moviesMatchingCriteria = this.moviesList;
                  }
              } else {
                  if (criteria) {
                      this.moviesMatchingCriteria = this.moviesList.filter(
                          (movie) => movie.name.toLowerCase().indexOf(criteria) !== -1 && movie.genres.indexOf(this.currentGenre) !== -1);
                  } else {
                      this.moviesMatchingCriteria = this.moviesList.filter(
                          (movie) => movie.genres.indexOf(this.currentGenre) !== -1);
                  }
              }

          });
      // detects changes in genre
      this.genreCriteriaChanged
          .debounceTime(300) // wait 300ms after the last event before emitting last event
          .distinctUntilChanged() // only emit if value is different from previous value
          .subscribe(genre => {
              if (!this.currentSearchCriteria) {
                  if (genre) {
                      this.moviesMatchingCriteria = this.moviesList.filter(
                          (movie) => movie.genres.indexOf(genre) !== -1);
                  } else {
                      this.moviesMatchingCriteria = this.moviesList;
                  }
              } else {
                  if (genre) {
                      this.moviesMatchingCriteria = this.moviesList.filter(
                          (movie) => movie.genres.indexOf(genre) !== -1 && movie.name.toLowerCase().indexOf(this.currentSearchCriteria) !== -1);
                  } else {
                      this.moviesMatchingCriteria = this.moviesList.filter(
                          (movie) => movie.name.toLowerCase().indexOf(this.currentSearchCriteria) !== -1);
                  }
              }
          });
  }

    /**
     * updates search criteria value with a new one
     * @param {string} criteria - search criteria
     * @returns void
     */
  public changeSearchCriteria(criteria: string): void {
    this.searchCriteriaChanged.next(criteria);
    this.currentSearchCriteria = criteria;
  }

    /**
     * updates genre filter value
     * @param {string} genre - movie genre
     * @returns void
     */
  public changeGenre(genre: string): void {
      this.genreCriteriaChanged.next(genre);
      this.currentGenre = genre;
  }

    /**
     * init function
     * @returns void
     */
  public ngOnInit(): void {
    this.getAllMovies();
  }

    /**
     * gets list of all movies
     * @returns void
     */
  public getAllMovies(): void {
    this.subscriptions.push(
        this._api.getMovies().subscribe(
            (movies) => {
              this.moviesMatchingCriteria = movies;
              this.moviesList = movies;
            },
            (error) => {
               console.log(error.error.message);
            }
        )
    );
  }

    /**
     * destroys all subscriptions
     * @returns void
     */
  public ngOnDestroy(): void {
    this.subscriptions.forEach((s) => s.unsubscribe());
  }

}
