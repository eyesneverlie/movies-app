export class IMovie {
    id: number;
    key: string;
    name: string;
    description: string;
    genres: string[];
    rate: string;
    length: string;
    img: string;
    likes: number;
}
