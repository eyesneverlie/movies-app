# movies-app

##### Project deployment
1. run git clone https://gitlab.com/eyesneverlie/movies-app.git
2. open movies-app folder and run npm install
3. after all packages have been installed run json-server db.json to run backend server on the local machine
4. run ng serve to run angular app
